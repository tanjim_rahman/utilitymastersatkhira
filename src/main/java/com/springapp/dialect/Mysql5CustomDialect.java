package com.springapp.dialect;

import org.hibernate.dialect.MySQL5Dialect;

/**
 * Created by tanjim.rahman on 28-Feb-18.
 */
public class Mysql5CustomDialect extends MySQL5Dialect {
    public Mysql5CustomDialect() {
        super();
        registerHibernateType(-5, "long");
    }
}
