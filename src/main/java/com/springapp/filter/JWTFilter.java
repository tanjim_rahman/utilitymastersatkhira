package com.springapp.filter;


import io.jsonwebtoken.Jwts;

import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.Key;

/**
 * Created by tanjim.rahman on 16-Aug-18.
 */
public class JWTFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        if(!containerRequestContext.getUriInfo().getPath().equalsIgnoreCase("user/sendOTP")
                && !containerRequestContext.getUriInfo().getPath().equalsIgnoreCase("user/validateOTP")
                && !containerRequestContext.getUriInfo().getPath().equalsIgnoreCase("ping")){
            try{
                String token = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
                String keySpec = "SAYON";
                Key key = new SecretKeySpec(keySpec.getBytes(), 0, keySpec.getBytes().length, "DES");
                Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            }catch (Exception e){
                containerRequestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
        }
    }
}
