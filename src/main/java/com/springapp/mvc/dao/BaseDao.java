package com.springapp.mvc.dao;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by esty on 10/20/17.
 */
public abstract class BaseDao {
    protected EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    protected Query persistenceQuery(String query){
        return em.createNativeQuery(query);
    }

    protected Query persistenceQuery(String query, Class entity){
        return em.createNativeQuery(query, entity);
    }

    protected Session getCurrentSession(){
        return em.unwrap(Session.class);
    }

    protected org.hibernate.Query hibernateQuery(String query){
        return getCurrentSession().createSQLQuery(query);
    }

    protected org.hibernate.Query hibernateQuery(String query, Class dto){
        return getCurrentSession().createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(dto));
    }

    protected String javaToMysqlDate(Date date){
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        String mysqlDateString = formatter.format(date);
        return mysqlDateString;
    }

}
