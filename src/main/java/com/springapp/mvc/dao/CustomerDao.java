package com.springapp.mvc.dao;

import com.springapp.mvc.entity.Customer;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
public class CustomerDao extends BaseDao implements ICustomerDao{

    @Override
    @Transactional(readOnly = true)
    public Customer getCustomer(Long customerID) {
        return em.find(Customer.class, customerID);
    }
}
