package com.springapp.mvc.dao;

import com.springapp.mvc.entity.Customer;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
public interface ICustomerDao {

    public Customer getCustomer(Long customerID);
}
