package com.springapp.mvc.dao;

import com.springapp.mvc.entity.MeterUserOTP;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public interface IMeterReaderUserDao {

    /*public MeterReaderUserDTO getMeterReaderUserDTO(String userName);

    public MeterReaderUser persist(MeterReaderUser meterReaderUser);*/

    public MeterUserOTP getMeterUserOtp(String msisdn);

    public MeterUserOTP mergeMeterUserOtp(MeterUserOTP meterUserOTP);
}
