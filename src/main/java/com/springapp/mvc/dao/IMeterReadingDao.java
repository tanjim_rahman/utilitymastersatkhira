package com.springapp.mvc.dao;

import com.springapp.mvc.dto.MeterInformationDTO;
import com.springapp.mvc.dto.MeterReaderDTO;
import com.springapp.mvc.dto.MeterReadingHistoryDTO;
import com.springapp.mvc.entity.ImageLog;
import com.springapp.mvc.entity.MeterReading;
import com.springapp.mvc.entity.MeterReadingBuffer;

import java.util.Date;
import java.util.List;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public interface IMeterReadingDao {

    public MeterReaderDTO getMeterReaderDTO(String mobileNO);

    public List<MeterInformationDTO> getMeterInformationList(Date systemDate, String mobileNO);

    public List<MeterInformationDTO> getMeterInformationList(String holdingNo);

    public List<MeterReadingHistoryDTO> getReadingHistoryDTOList(Date fromDate, Date toDate, String mobileNO);

    public ImageLog getImageLog(Long imageID);

    public ImageLog mergeImageLog(ImageLog imageLog);

    public MeterReading persistMeterReading(MeterReading meterReading);

    public MeterReadingBuffer persistMeterReadingBuffer(MeterReadingBuffer meterReadingBuffer);
}
