package com.springapp.mvc.dao;

import com.springapp.mvc.dto.MeterReaderUserDTO;
import com.springapp.mvc.entity.MeterReaderUser;
import com.springapp.mvc.entity.MeterUserOTP;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class MeterReaderUserDao extends BaseDao implements IMeterReaderUserDao {

    /*@Override
    @Transactional(readOnly = true)
    public MeterReaderUserDTO getMeterReaderUserDTO(String userName) {
        org.hibernate.Query hQuery = hibernateQuery("SELECT A.UserName AS userName, A.Password AS password, A.FullName AS fullName, A.Status AS status FROM meterreaderuser A WHERE A.UserName = :userName", MeterReaderUserDTO.class)
                .setParameter("userName", userName);
        List<MeterReaderUserDTO> meterReaderUserDTOList = hQuery.list();
        return meterReaderUserDTOList.isEmpty() ? null : (MeterReaderUserDTO) hQuery.list().get(0);
    }

    @Override
    public MeterReaderUser persist(MeterReaderUser meterReaderUser) {
        em.persist(meterReaderUser);
        return meterReaderUser;
    }*/

    @Override
    @Transactional(readOnly = true)
    public MeterUserOTP getMeterUserOtp(String msisdn) {
        return em.find(MeterUserOTP.class, msisdn);
    }

    @Override
    public MeterUserOTP mergeMeterUserOtp(MeterUserOTP meterUserOTP) {
        return em.merge(meterUserOTP);
    }
}
