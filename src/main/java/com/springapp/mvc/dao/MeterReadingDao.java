package com.springapp.mvc.dao;

import com.springapp.mvc.dto.MeterInformationDTO;
import com.springapp.mvc.dto.MeterReaderDTO;
import com.springapp.mvc.dto.MeterReadingHistoryDTO;
import com.springapp.mvc.entity.ImageLog;
import com.springapp.mvc.entity.MeterReading;
import com.springapp.mvc.entity.MeterReadingBuffer;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.Date;
import java.util.List;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class MeterReadingDao extends BaseDao implements IMeterReadingDao {

    @Override
    @Transactional(readOnly = true)
    public MeterReaderDTO getMeterReaderDTO(String mobileNO) {
        org.hibernate.Query hQuery = hibernateQuery("SELECT A.EmployeeID AS employeeID, A.EmployeeName AS employeeName, A.MobileNo AS mobileNo FROM employee A WHERE A.MobileNo = :mobileNO", MeterReaderDTO.class);
        hQuery.setParameter("mobileNO", mobileNO);
        List<MeterReaderDTO> meterReaderDTOList = hQuery.list();
        return meterReaderDTOList.isEmpty() ? null : meterReaderDTOList.get(0);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeterInformationDTO> getMeterInformationList(Date systemDate, String mobileNO) {
        org.hibernate.Query hQuery = hibernateQuery("SELECT B.CustomerID AS customerID, B.Name AS name, B.HoldingNo AS holdingNo, B.ClientNo AS clientNo, B.Address AS address, B.MeterNo AS meterNo, Z.ReadingDate AS lastReadingDate, Z.UnitRead AS lastUnitRead " +
                "FROM meterreadschedule A " +
                "INNER JOIN customer B ON A.HoldingNo = B.HoldingNo " +
                "INNER JOIN employee C ON A.EmployeeID = C.EmployeeID " +
                "LEFT JOIN " +
                "( " +
                "SELECT * FROM " +
                "( " +
                "SELECT A.MeterNo, A.ReadingDate, A.UnitRead " +
                ", row_number() OVER(PARTITION BY A.MeterNo ORDER BY A.MeterNo, A.ReadingDate DESC) AS RN " +
                "FROM meterreading A " +
                ") X WHERE X.RN = 1 " +
                ") Z ON B.MeterNo = Z.MeterNo " +
                "WHERE C.MobileNo = :mobileNO AND A.DueDate = :systemDate", MeterInformationDTO.class);
        hQuery.setParameter("systemDate", javaToMysqlDate(systemDate));
        hQuery.setParameter("mobileNO", mobileNO);
        return hQuery.list();
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeterInformationDTO> getMeterInformationList(String holdingNo) {
        org.hibernate.Query hQuery = hibernateQuery("SELECT A.CustomerID AS customerID, A.Name AS name, A.HoldingNo AS holdingNo, A.ClientNo AS clientNo, A.Address AS address, A.MeterNo AS meterNo, Z.ReadingDate AS lastReadingDate, Z.UnitRead AS lastUnitRead " +
                "FROM customer A " +
                "LEFT JOIN " +
                "( " +
                "SELECT * FROM " +
                "( " +
                "SELECT A.MeterNo, A.ReadingDate, A.UnitRead " +
                ", row_number() OVER(PARTITION BY A.MeterNo ORDER BY A.MeterNo, A.ReadingDate DESC) AS RN " +
                "FROM meterreading A " +
                ") X WHERE X.RN = 1 " +
                ") Z ON A.MeterNo = Z.MeterNo " +
                "WHERE A.HoldingNo = :holdingNo", MeterInformationDTO.class);
        hQuery.setParameter("holdingNo", holdingNo);
        return hQuery.list();
    }

    @Override
    @Transactional(readOnly = true)
    public List<MeterReadingHistoryDTO> getReadingHistoryDTOList(Date fromDate, Date toDate, String mobileNO) {
        org.hibernate.Query hQuery = hibernateQuery("SELECT B.CustomerID AS customerID, A.MeterNo AS meterNo, B.name AS name, B.holdingNo AS holdingNo, B.clientNo AS clientNo, B.address AS address " +
                ", A.ReadingDate AS readingDate, A.Latitude AS latitude, A.Longitude AS longitude, A.UnitRead AS unitRead, A.CommentID AS commentID, A.OtherComment AS otherComment " +
                "FROM " +
                "( " +
                "SELECT A.EmployeeID, A.MeterNo, A.ReadingDate, A.Latitude, A.Longitude, A.UnitRead, A.CommentID, A.OtherComment FROM meterreading A " +
                "UNION ALL " +
                "SELECT A.EmployeeID, A.MeterNo, A.ReadingDate, A.Latitude, A.Longitude, null, A.CommentID, A.OtherComment FROM meterreadingbuffer A " +
                ") AS A " +
                "INNER JOIN customer B ON A.MeterNo = B.MeterNo " +
                "INNER JOIN employee C ON A.EmployeeID = C.EmployeeID " +
                "WHERE C.MobileNo = :mobileNO AND A.ReadingDate BETWEEN :fromDate AND :toDate", MeterReadingHistoryDTO.class);
        hQuery.setParameter("fromDate", fromDate);
        hQuery.setParameter("toDate", toDate);
        hQuery.setParameter("mobileNO", mobileNO);
        return hQuery.list();
    }

    @Override
    public ImageLog getImageLog(Long imageID) {
        return em.find(ImageLog.class, imageID, LockModeType.PESSIMISTIC_WRITE);
    }

    @Override
    public ImageLog mergeImageLog(ImageLog imageLog) {
        return em.merge(imageLog);
    }

    @Override
    public MeterReading persistMeterReading(MeterReading meterReading) {
        em.persist(meterReading);
        return meterReading;
    }

    @Override
    public MeterReadingBuffer persistMeterReadingBuffer(MeterReadingBuffer meterReadingBuffer) {
        em.persist(meterReadingBuffer);
        return meterReadingBuffer;
    }
}
