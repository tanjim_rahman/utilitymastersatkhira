package com.springapp.mvc.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@XmlRootElement
public class MeterInformationDTO implements Serializable {

    @XmlElement
    private Long customerID;
    @XmlElement
    private Integer meterNo;
    @XmlElement
    private String name;
    @XmlElement
    private String holdingNo;
    @XmlElement
    private String clientNo;
    @XmlElement
    private String address;
    @XmlElement
    private Date lastReadingDate;
    @XmlElement
    private Integer lastUnitRead;

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Integer getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(Integer meterNo) {
        this.meterNo = meterNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getLastReadingDate() {
        return lastReadingDate;
    }

    public void setLastReadingDate(Date lastReadingDate) {
        this.lastReadingDate = lastReadingDate;
    }

    public Integer getLastUnitRead() {
        return lastUnitRead;
    }

    public void setLastUnitRead(Integer lastUnitRead) {
        this.lastUnitRead = lastUnitRead;
    }
}
