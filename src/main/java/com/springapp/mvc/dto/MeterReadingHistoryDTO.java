package com.springapp.mvc.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@XmlRootElement
public class MeterReadingHistoryDTO implements Serializable {

    @XmlElement
    private Long customerID;
    @XmlElement
    private Integer meterNo;
    @XmlElement
    private String name;
    @XmlElement
    private String holdingNo;
    @XmlElement
    private String clientNo;
    @XmlElement
    private String address;
    @XmlElement
    private Date readingDate;
    @XmlElement
    private BigDecimal latitude;
    @XmlElement
    private BigDecimal longitude;
    @XmlElement
    private Integer unitRead;
    @XmlElement
    private Integer commentID;
    @XmlElement
    private String otherComment;

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Integer getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(Integer meterNo) {
        this.meterNo = meterNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(Date readingDate) {
        this.readingDate = readingDate;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Integer getUnitRead() {
        return unitRead;
    }

    public void setUnitRead(Integer unitRead) {
        this.unitRead = unitRead;
    }

    public Integer getCommentID() {
        return commentID;
    }

    public void setCommentID(Integer commentID) {
        this.commentID = commentID;
    }

    public String getOtherComment() {
        return otherComment;
    }

    public void setOtherComment(String otherComment) {
        this.otherComment = otherComment;
    }
}
