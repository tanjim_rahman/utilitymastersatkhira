package com.springapp.mvc.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@XmlRootElement
public class ResponseDTO implements Serializable{
    @XmlElement
    private Integer code;
    @XmlElement
    private String msg;
    @XmlElement
    private Object data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
