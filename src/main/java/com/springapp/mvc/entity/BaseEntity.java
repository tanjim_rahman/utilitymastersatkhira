package com.springapp.mvc.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by esty on 10/23/17.
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    @Column(name = "CreatedBy")
    private Integer createdBy;

    @Column(name = "ServerDate")
    private Date serverDate;

    @Column(name = "SystemDate")
    private Date systemDate;

    @Version
    @Column(name = "Version")
    private Integer version;

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getServerDate() {
        return serverDate;
    }

    public void setServerDate(Date serverDate) {
        this.serverDate = serverDate;
    }

    public Date getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(Date systemDate) {
        this.systemDate = systemDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
