package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "customer")
public class Customer extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CustomerID")
    private Long customerID;

    @Column(name = "HoldingNo")
    private String holdingNo;

    @Column(name = "ClientNo")
    private String clientNo;

    @Column(name = "Name")
    private String name;

    @Column(name = "FatherName")
    private String fatherName;

    @Column(name = "Address")
    private String address;

    @Column(name = "GeoAreaID")
    private Integer geoAreaID;

    @Column(name = "meterNo")
    private Integer meterNo;

    @Column(name = "Status")
    private Integer status;

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public String getClientNo() {
        return clientNo;
    }

    public void setClientNo(String clientNo) {
        this.clientNo = clientNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getGeoAreaID() {
        return geoAreaID;
    }

    public void setGeoAreaID(Integer geoAreaID) {
        this.geoAreaID = geoAreaID;
    }

    public Integer getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(Integer meterNo) {
        this.meterNo = meterNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
