package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "designation")
public class Designation extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DesignationID")
    private Integer designationID;

    @Column(name = "DesignationName")
    private String designationName;

    @Column(name = "Status")
    private Integer status;

    public Integer getDesignationID() {
        return designationID;
    }

    public void setDesignationID(Integer designationID) {
        this.designationID = designationID;
    }

    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
