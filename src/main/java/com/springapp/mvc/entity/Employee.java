package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "employee")
public class Employee extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EmployeeID")
    private Integer employeeID;

    @Column(name = "EmployeeName")
    private String employeeName;

    @Column(name = "DesignationID")
    private Integer designationID;

    @Column(name = "MobileNo")
    private String mobileNo;

    @Column(name = "Status")
    private Integer status;

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getDesignationID() {
        return designationID;
    }

    public void setDesignationID(Integer designationID) {
        this.designationID = designationID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
