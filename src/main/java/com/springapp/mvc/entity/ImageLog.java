package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "imagelog")
public class ImageLog implements Serializable {

    @Id
    @Column(name = "ImageID")
    private Long imageID;

    @Column(name = "ImageSL")
    private Long ImageSL;

    public Long getImageID() {
        return imageID;
    }

    public void setImageID(Long imageID) {
        this.imageID = imageID;
    }

    public Long getImageSL() {
        return ImageSL;
    }

    public void setImageSL(Long imageSL) {
        ImageSL = imageSL;
    }
}
