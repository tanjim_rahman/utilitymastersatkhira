package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "meterreadschedule")
public class MeterReadSchedule extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ScheduleID")
    private Long scheduleID;

    @Column(name = "EmployeeID")
    private Integer employeeID;

    @Column(name = "HoldingNo")
    private String holdingNo;

    @Column(name = "DueDate")
    private Date dueDate;

    @Column(name = "Status")
    private Integer status;

    public Long getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(Long scheduleID) {
        this.scheduleID = scheduleID;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
