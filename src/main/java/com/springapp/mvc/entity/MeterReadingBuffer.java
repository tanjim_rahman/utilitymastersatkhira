package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/3/2018.
 */
@Entity
@Table(name = "meterreadingbuffer")
public class MeterReadingBuffer extends BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ReadingID")
    private Long readingID;

    @Column(name = "meterNo")
    private Integer meterNo;

    @Column(name = "EmployeeID")
    private Integer employeeID;

    @Column(name = "ReadingDate")
    private Date readingDate;

    @Column(name = "Latitude")
    private BigDecimal latitude;

    @Column(name = "Longitude")
    private BigDecimal longitude;

    @Column(name = "ImageID")
    private Long imageID;

    @Column(name = "ReadingMonth")
    private Integer readingMonth;

    @Column(name = "ReadingYear")
    private Integer readingYear;

    @Column(name = "CommentID")
    private Integer commentID;

    @Column(name = "OtherComment")
    private String otherComment;

    @Column(name = "Status")
    private Integer status;

    public Long getReadingID() {
        return readingID;
    }

    public void setReadingID(Long readingID) {
        this.readingID = readingID;
    }

    public Integer getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(Integer meterNo) {
        this.meterNo = meterNo;
    }

    public Integer getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        this.employeeID = employeeID;
    }

    public Date getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(Date readingDate) {
        this.readingDate = readingDate;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Long getImageID() {
        return imageID;
    }

    public void setImageID(Long imageID) {
        this.imageID = imageID;
    }

    public Integer getReadingMonth() {
        return readingMonth;
    }

    public void setReadingMonth(Integer readingMonth) {
        this.readingMonth = readingMonth;
    }

    public Integer getReadingYear() {
        return readingYear;
    }

    public void setReadingYear(Integer readingYear) {
        this.readingYear = readingYear;
    }

    public Integer getCommentID() {
        return commentID;
    }

    public void setCommentID(Integer commentID) {
        this.commentID = commentID;
    }

    public String getOtherComment() {
        return otherComment;
    }

    public void setOtherComment(String otherComment) {
        this.otherComment = otherComment;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
