package com.springapp.mvc.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@Entity
@Table(name = "meteruserotp")
public class MeterUserOTP implements Serializable {

    @Id
    @Column(name = "Msisdn")
    private String msisdn;

    @Column(name = "OTP")
    private String otp;

    @Column(name = "ServerDate")
    private Date serverDate;

    @Version
    @Column(name = "Version")
    private Integer version;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Date getServerDate() {
        return serverDate;
    }

    public void setServerDate(Date serverDate) {
        this.serverDate = serverDate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
