package com.springapp.mvc.enumeration;

/**
 * Created by tanjim.rahman on 18-Jul-18.
 */
public enum GlobalStatus {
    Active(1),
    Inactive(0);

    private final int value;

    GlobalStatus(int value) {
        this.value = value;
    }

    public  int value(){
        return value;
    }
}
