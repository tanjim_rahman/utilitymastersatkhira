package com.springapp.mvc.enumeration;

/**
 * Created by tanjim.esty on 10/7/2018.
 */
public enum MiscellniousLong {
    ONE(1);

    private final long value;

    MiscellniousLong(long value) {
        this.value = value;
    }

    public  long value(){
        return value;
    }
}
