package com.springapp.mvc.service;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by tanjim.esty on 10/7/2018.
 */
public abstract class BaseService implements MessageSourceAware {

    private MessageSource messageSource;

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    protected String bsGetMessages(String msgCode, String[] params){
        Locale locale = new Locale("bn", "BN");
        String msg = messageSource.getMessage(msgCode, params, locale);
        return msg;
    }

    protected Date bsConvertDateFromString(String date) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd", Locale.ENGLISH);
        Date returnDate = format.parse(date);
        return returnDate;
    }
}
