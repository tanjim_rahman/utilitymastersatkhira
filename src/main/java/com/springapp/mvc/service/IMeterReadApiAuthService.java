package com.springapp.mvc.service;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@Path("/user/")
public interface IMeterReadApiAuthService {

    @POST
    @Path("/sendOTP/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendOTP(@Multipart(value="mobileNo", required = true) String mobileNo);

    @POST
    @Path("/validateOTP/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateOTP(@Multipart(value="mobileNo", required = true) String mobileNo,
                                @Multipart(value="otp", required = true) String otp);
}
