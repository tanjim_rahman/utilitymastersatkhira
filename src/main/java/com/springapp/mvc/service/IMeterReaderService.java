package com.springapp.mvc.service;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
@Path("/meterreading/")
public interface IMeterReaderService {

    @POST
    @Path("/getMeterList/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeterList(@Multipart(value="mobileNo", required = true) String mobileNo,
                                 @Multipart(value="scheduleDate", required = true) String scheduleDate);

    @POST
    @Path("/getMeterListByHoldingNo/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeterListByHoldingNo(@Multipart(value="holdingNo", required = true) String holdingNo);

    @POST
    @Path("/uploadReading/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadReading(@Multipart(value="meterNo", required = true) Integer meterNo,
                                  @Multipart(value="readingDate", required = true) String readingDate,
                                  @Multipart(value="latitude", required = true) BigDecimal latitude,
                                  @Multipart(value="longitude", required = true) BigDecimal longitude,
                                  @Multipart(value="unitRead", required = true) Integer unitRead,
                                  @Multipart(value="commentID", required = true) Integer commentID,
                                  @Multipart(value="otherComment", required = false) String otherComment,
                                  @Multipart(value="mobileNO", required = true) String mobileNO,
                                  @Multipart(value="readingMonth", required = true) Integer readingMonth,
                                  @Multipart(value="readingYear", required = true) Integer readingYear,
                                  @Multipart(value="image", required = true) Attachment attachment);

    @POST
    @Path("/uploadReadingBuffer/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadReadingBuffer(@Multipart(value="meterNo", required = true) Integer meterNo,
                                  @Multipart(value="readingDate", required = true) String readingDate,
                                  @Multipart(value="latitude", required = true) BigDecimal latitude,
                                  @Multipart(value="longitude", required = true) BigDecimal longitude,
                                  @Multipart(value="commentID", required = true) Integer commentID,
                                  @Multipart(value="otherComment", required = true) String otherComment,
                                  @Multipart(value="mobileNO", required = true) String mobileNO,
                                  @Multipart(value="readingMonth", required = true) Integer readingMonth,
                                  @Multipart(value="readingYear", required = true) Integer readingYear,
                                  @Multipart(value="image", required = true) Attachment attachment);

    @POST
    @Path("/getReadingHistory/")
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReadingHistory(@Multipart(value="mobileNO", required = true) String mobileNO,
                                        @Multipart(value="fromDate", required = true) String fromDate,
                                        @Multipart(value="toDate", required = true) String toDate);
}
