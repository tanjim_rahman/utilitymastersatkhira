package com.springapp.mvc.service;

import java.security.NoSuchAlgorithmException;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public interface IOTPHandler {

    public String getOTP(int size) throws NoSuchAlgorithmException;

    public void saveOTP(String msisdn, String otp);

    public Boolean validateOTP(String msisdn, String otp);
}
