package com.springapp.mvc.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by tanjim.esty on 10/3/2018.
 */

@Path("/ping/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface IPingService {

    /*@POST
    @Path("/auth/")
    public Response auth(MeterReaderAuthDTO meterReaderAuthDTO);*/

    /*@POST
    @Path("/register/")
    public Response register(MeterReaderUserDTO meterReaderUserDTO);*/
}
