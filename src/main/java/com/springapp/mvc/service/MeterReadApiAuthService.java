package com.springapp.mvc.service;

import com.springapp.mvc.dao.IMeterReadingDao;
import com.springapp.mvc.dto.MeterReaderDTO;
import com.springapp.mvc.dto.ResponseDTO;
import com.springapp.utility.ITokenGenerator;
import org.springframework.http.HttpStatus;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.net.URLEncoder;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class MeterReadApiAuthService extends BaseService implements IMeterReadApiAuthService {

    private IMeterReadingDao meterReadingDao;
    private IOTPHandler otpHandler;
    private ITokenGenerator tokenGenerator;

    public void setMeterReadingDao(IMeterReadingDao meterReadingDao) {
        this.meterReadingDao = meterReadingDao;
    }

    public void setOtpHandler(IOTPHandler otpHandler) {
        this.otpHandler = otpHandler;
    }

    public void setTokenGenerator(ITokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
    }

    @Override
    public Response sendOTP(String mobileNo) {
        try {
            MeterReaderDTO meterReaderDTO = meterReadingDao.getMeterReaderDTO(mobileNo);
            if(meterReaderDTO == null){
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setCode(HttpStatus.FORBIDDEN.value());
                responseDTO.setMsg("Mobile no is not registered. Please check.");
                //responseDTO.setMsg(bsGetMessages("meterReaderApp.mobileNoNotRegistered", null));
                responseDTO.setData(null);

                return Response.ok().entity(responseDTO).build();
            }

            String otp = otpHandler.getOTP(4);
            String sms = "Your one time password is " + otp;
            if (sms != null) {
                Client client = ClientBuilder.newBuilder().newClient();
                // TODO: csmsid uniqueness
                WebTarget target = client.target("http://sms.sslwireless.com/pushapi/dynamic/server.php?msisdn=" + mobileNo + "&sms="+ URLEncoder.encode(sms) +"&user=tanjim.esty&pass=s@81B987&sid=SSLTESTAPI&csmsid=123456789");

                Invocation.Builder builder = target.request();
                Response response = builder.get();

                response.close();
                client.close();

                otpHandler.saveOTP(mobileNo, otp);
            }

            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("You will receive an one time password soon.");
            responseDTO.setData(null);

            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response validateOTP(String mobileNo, String otp) {
        try {
            if (otpHandler.validateOTP(mobileNo, otp)) {
                String token = tokenGenerator.getToken();
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setCode(HttpStatus.OK.value());
                responseDTO.setMsg("Login successful.");
                MeterReaderDTO meterReaderDTO = meterReadingDao.getMeterReaderDTO(mobileNo);
                responseDTO.setData(meterReaderDTO);
                return Response.ok().entity(responseDTO).header(HttpHeaders.AUTHORIZATION, token).build();
            } else {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
