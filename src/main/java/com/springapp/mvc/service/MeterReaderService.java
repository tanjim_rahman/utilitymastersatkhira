package com.springapp.mvc.service;

import com.springapp.mvc.dao.IMeterReadingDao;
import com.springapp.mvc.dto.MeterInformationDTO;
import com.springapp.mvc.dto.MeterReaderDTO;
import com.springapp.mvc.dto.MeterReadingHistoryDTO;
import com.springapp.mvc.dto.ResponseDTO;
import com.springapp.mvc.entity.ImageLog;
import com.springapp.mvc.entity.MeterReading;
import com.springapp.mvc.entity.MeterReadingBuffer;
import com.springapp.mvc.enumeration.GlobalStatus;
import com.springapp.mvc.enumeration.MiscellniousLong;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;

import javax.activation.DataHandler;
import javax.ws.rs.core.Response;
import java.io.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class MeterReaderService extends BaseService implements IMeterReaderService {

    private IMeterReadingDao meterReadingDao;

    public void setMeterReadingDao(IMeterReadingDao meterReadingDao) {
        this.meterReadingDao = meterReadingDao;
    }

    @Override
    public Response getMeterList(String mobileNo, String scheduleDate) {
        try {
            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("Download successful.");
            List<MeterInformationDTO> meterInformationDTOList = meterReadingDao.getMeterInformationList(bsConvertDateFromString(scheduleDate), mobileNo);
            responseDTO.setData(meterInformationDTOList);
            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response getMeterListByHoldingNo(String holdingNo) {
        try {
            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("Download successful.");
            List<MeterInformationDTO> meterInformationDTOList = meterReadingDao.getMeterInformationList(holdingNo);
            responseDTO.setData(meterInformationDTOList);
            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Response uploadReading(Integer meterNo, String readingDate, BigDecimal latitude, BigDecimal longitude, Integer unitRead,
                                  Integer commentID, String otherComment, String mobileNO, Integer readingMonth, Integer readingYear, Attachment attachment) {
        try {
            MeterReaderDTO meterReaderDTO = meterReadingDao.getMeterReaderDTO(mobileNO);
            if(meterReaderDTO == null){
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setCode(HttpStatus.FORBIDDEN.value());
                responseDTO.setMsg("Mobile no is not registered. Please check.");
                responseDTO.setData(null);

                return Response.ok().entity(responseDTO).build();
            }

            ImageLog imageLog = meterReadingDao.getImageLog(MiscellniousLong.ONE.value());
            if(imageLog == null) {
                imageLog = new ImageLog();
                imageLog.setImageID(MiscellniousLong.ONE.value());
                imageLog.setImageSL(MiscellniousLong.ONE.value());
            }else{
                imageLog.setImageSL(imageLog.getImageSL() + 1);
            }

            Long imageSL = imageLog.getImageSL();
            meterReadingDao.mergeImageLog(imageLog);

            MeterReading meterReading = new MeterReading();
            meterReading.setMeterNo(meterNo);
            meterReading.setEmployeeID(meterReaderDTO.getEmployeeID());
            meterReading.setReadingDate(bsConvertDateFromString(readingDate));
            meterReading.setLatitude(latitude);
            meterReading.setLongitude(longitude);
            meterReading.setImageID(imageSL);
            meterReading.setReadingMonth(readingMonth);
            meterReading.setReadingYear(readingYear);
            meterReading.setUnitRead(unitRead);
            meterReading.setCommentID(commentID);
            meterReading.setOtherComment(otherComment);
            meterReading.setStatus(GlobalStatus.Active.value());
            meterReading.setSystemDate(new Date());
            meterReading.setServerDate(new Date());
            meterReading.setCreatedBy(1);

            meterReadingDao.persistMeterReading(meterReading);

            DataHandler dataHandler = attachment.getDataHandler();
            InputStream inputStream = dataHandler.getInputStream();
            writeToFileServer(inputStream, imageSL.toString().concat(".jpg"));
            inputStream.close();

            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("Upload successful.");
            responseDTO.setData(null);
            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Response uploadReadingBuffer(Integer meterNo, String readingDate, BigDecimal latitude, BigDecimal longitude,
                                        Integer commentID, String otherComment, String mobileNO, Integer readingMonth, Integer readingYear, Attachment attachment) {
        try {
            MeterReaderDTO meterReaderDTO = meterReadingDao.getMeterReaderDTO(mobileNO);
            if(meterReaderDTO == null){
                ResponseDTO responseDTO = new ResponseDTO();
                responseDTO.setCode(HttpStatus.FORBIDDEN.value());
                responseDTO.setMsg("Mobile no is not registered. Please check.");
                responseDTO.setData(null);

                return Response.ok().entity(responseDTO).build();
            }

            ImageLog imageLog = meterReadingDao.getImageLog(MiscellniousLong.ONE.value());
            if(imageLog == null) {
                imageLog = new ImageLog();
                imageLog.setImageID(MiscellniousLong.ONE.value());
                imageLog.setImageSL(MiscellniousLong.ONE.value());
            }else{
                imageLog.setImageSL(imageLog.getImageSL() + 1);
            }

            Long imageSL = imageLog.getImageSL();
            meterReadingDao.mergeImageLog(imageLog);

            MeterReadingBuffer meterReadingBuffer = new MeterReadingBuffer();
            meterReadingBuffer.setMeterNo(meterNo);
            meterReadingBuffer.setEmployeeID(meterReaderDTO.getEmployeeID());
            meterReadingBuffer.setReadingDate(bsConvertDateFromString(readingDate));
            meterReadingBuffer.setLatitude(latitude);
            meterReadingBuffer.setLongitude(longitude);
            meterReadingBuffer.setImageID(imageSL);
            meterReadingBuffer.setReadingMonth(readingMonth);
            meterReadingBuffer.setReadingYear(readingYear);
            meterReadingBuffer.setCommentID(commentID);
            meterReadingBuffer.setOtherComment(otherComment);
            meterReadingBuffer.setStatus(GlobalStatus.Active.value());
            meterReadingBuffer.setSystemDate(new Date());
            meterReadingBuffer.setServerDate(new Date());
            meterReadingBuffer.setCreatedBy(1);

            meterReadingDao.persistMeterReadingBuffer(meterReadingBuffer);

            DataHandler dataHandler = attachment.getDataHandler();
            InputStream inputStream = dataHandler.getInputStream();
            writeToFileServer(inputStream, imageSL.toString().concat(".jpg"));
            inputStream.close();

            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("Upload successful.");
            responseDTO.setData(null);
            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response getReadingHistory(String mobileNO, String fromDate, String toDate) {
        try {
            ResponseDTO responseDTO = new ResponseDTO();
            responseDTO.setCode(HttpStatus.OK.value());
            responseDTO.setMsg("Download successful.");
            List<MeterReadingHistoryDTO> meterReadingHistoryDTOList = meterReadingDao.getReadingHistoryDTOList(bsConvertDateFromString(fromDate), bsConvertDateFromString(toDate), mobileNO);
            responseDTO.setData(meterReadingHistoryDTOList);
            return Response.ok().entity(responseDTO).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    private void writeToFileServer(InputStream inputStream, String fileName) throws IOException {
        OutputStream outputStream = null;
        //outputStream = new FileOutputStream(new File("/usr/share/robidemo/robiweb/utilitysatkhira/" + fileName));
        outputStream = new FileOutputStream(new File("/usr/share/robidemo/robiweb/utilitysatkhira/" + fileName));
        int read = 0;
        byte[] bytes = new byte[1024];
        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        outputStream.flush();
        outputStream.close();
    }
}
