package com.springapp.mvc.service;

import com.springapp.mvc.dao.IMeterReaderUserDao;
import com.springapp.mvc.entity.MeterUserOTP;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class OTPHandler implements IOTPHandler {

    private IMeterReaderUserDao meterReaderUserDao;

    public void setMeterReaderUserDao(IMeterReaderUserDao meterReaderUserDao) {
        this.meterReaderUserDao = meterReaderUserDao;
    }

    public String getOTP(int size) throws NoSuchAlgorithmException {

        StringBuilder generatedToken = new StringBuilder();
        try {
            SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
            // Generate 20 integers 0..20
            for (int i = 0; i < size; i++) {
                generatedToken.append(number.nextInt(9));
            }
        } catch (NoSuchAlgorithmException e) {
            throw e;
        }

        return generatedToken.toString();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOTP(String msisdn, String otp) {
        MeterUserOTP meterUserOTP = meterReaderUserDao.getMeterUserOtp(msisdn);
        if (meterUserOTP == null) {
            meterUserOTP = new MeterUserOTP();
            meterUserOTP.setMsisdn(msisdn);
        }
        meterUserOTP.setServerDate(new Date());
        meterUserOTP.setOtp(otp);
        meterReaderUserDao.mergeMeterUserOtp(meterUserOTP);
    }

    @Override
    public Boolean validateOTP(String msisdn, String otp) {
        MeterUserOTP meterUserOTP = meterReaderUserDao.getMeterUserOtp(msisdn);
        if (otp.equals(meterUserOTP.getOtp())) {
            return true;
        }
        return false;
    }
}
