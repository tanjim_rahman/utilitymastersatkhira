package com.springapp.mvc.web;

import com.springapp.mvc.dao.ICustomerDao;
import com.springapp.mvc.entity.Customer;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/")
public class HelloController {

    private ICustomerDao customerDao;

    public void setCustomerDao(ICustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
        Customer customer = customerDao.getCustomer((long) 1);
		model.addAttribute("message", "Welcome to Satkhira");
		return "hello";
	}
}