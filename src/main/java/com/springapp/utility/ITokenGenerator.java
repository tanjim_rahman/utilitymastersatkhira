package com.springapp.utility;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public interface ITokenGenerator {

    public String getToken();
}
