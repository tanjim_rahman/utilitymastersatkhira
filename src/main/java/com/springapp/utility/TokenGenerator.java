package com.springapp.utility;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;

/**
 * Created by tanjim.esty on 10/4/2018.
 */
public class TokenGenerator implements ITokenGenerator {

    @Override
    public String getToken() {
        String keySpec = "SAYON";
        Key key = new SecretKeySpec(keySpec.getBytes(), 0, keySpec.getBytes().length, "DES");
        String token = Jwts.builder()
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 86400000))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
        return token;
    }
}
